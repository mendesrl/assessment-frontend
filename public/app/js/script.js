var categorias = "";
var itensEmExibicao = "";
var coresParaFiltro = [
  { id: 0, nome: "Preta", classe: "sidebar__btn sidebar__btn--black" },
  { id: 1, nome: "Laranja", classe: "sidebar__btn sidebar__btn--orange" },
  { id: 2, nome: "Amarela", classe: "sidebar__btn sidebar__btn--yellow" },
  { id: 3, nome: "Rosa", classe: "sidebar__btn sidebar__btn--pink" },
  { id: 4, nome: "Azul", classe: "sidebar__btn sidebar__btn--blue" },
  { id: 5, nome: "Bege", classe: "sidebar__btn sidebar__btn--beige" },
  { id: 6, nome: "cinza", classe: "sidebar__btn sidebar__btn--grey" },
];
var tiposDeFiltro = [
  { id: 0, nome: "Corrida" },
  { id: 1, nome: "Caminhada" },
  { id: 2, nome: "Casual" },
  { id: 3, nome: "Social" },
];

obterCategorias();
renderizarFiltroPorCor();
renderizarFiltroPorTipo();

function renderizarFiltroPorCor() {
  let botoesParaRenderizar = "";
  for (let cor of coresParaFiltro) {
    botoesParaRenderizar += `
    <button class="${cor.classe} 
    aria-label="filtro de cor ${cor.nome} 
    onclick="filtrarPorCor('${cor.nome}')">    
    </button>`;
  }
  document.getElementById("filtros_de_cor").innerHTML = botoesParaRenderizar;
}

function renderizarFiltroPorTipo() {
  let botoesParaRenderizar = "";
  for (let tipo of tiposDeFiltro) {
    botoesParaRenderizar += `
    <li>
        <button onclick="buscar('${tipo.nome}')">
            ${tipo.nome}
        </button>
    </li>`;
  }
  document.getElementById("filtros_de_tipo").innerHTML = botoesParaRenderizar;
}
async function obterCategorias() {
  try {
    const response = await fetch(
      "http://localhost:8888/api/V1/categories/list"
    );

    const data = await response.json();
    categorias = data.items;
    exibirCategorias(categorias);
    carregarDados(categorias[0].id);
  } catch (error) {
    console.error(error);
  }
}

function exibirCategorias(categorias) {
  let listagemDeCategorias = "";
  for (let categoria of categorias) {
    listagemDeCategorias += `
    <li>
        <button onclick="filtrarPorCategoria(${categoria.id})">
            ${categoria.name}
        </button>
    </li>`;
  }
  document.getElementById("categoria").innerHTML = listagemDeCategorias;
}

function carregarDados(idCategoria) {
  categorias.forEach((element) => {
    if (element.id === idCategoria) {
      document.getElementsByClassName(
        "section__title"
      )[0].innerHTML = `<span>${element.name}</span>`;
    }
  });

  obterItensFiltradosPorCategoria(idCategoria);
}

function filtrarPorCategoria(idCategoria) {
  carregarDados(idCategoria);
}

function filtrarPorCor(nomeCor) {
  let itensParaRenderizar = "";

  this.itensEmExibicao.filter((item) => {
    item.filter.filter((name) => {
      if (name.color) {
        let itemEncontrado = name.color
          .toLowerCase()
          .includes(nomeCor.toLowerCase());
        if (itemEncontrado) {
          itensParaRenderizar += `<div class="card">
            <div class="card__border" id="img-item">
                <img class="card__img" src="${item.image}" alt="${item.name}"/>
            </div>
            <div class="card__item">
                <div class="card__item-name">
                    <span>${item.name}</span>
                </div>
                <div class="card__item-value">
                    <span>R$${item.price}</span>
                </div>
                <div class="card__item-buy">
                  <button>COMPRAR</button>
                </div>
            </div>
        </div>`;
        }
      }
    });

    return (document.getElementById("itens").innerHTML = itensParaRenderizar);
  });
}

async function obterItensFiltradosPorCategoria(idCategoria) {
  try {
    const response = await fetch(
      `http://localhost:8888/api/V1/categories/${idCategoria}`
    );

    const data = await response.json();
    itensEmExibicao = data.items;
    exibirItensFiltradosNaPagina(itensEmExibicao);
  } catch (error) {
    console.error(error);
  }
}

function exibirItensFiltradosNaPagina(itens) {
  let itensParaRenderizar = "";
  itens.forEach((element) => {
    itensParaRenderizar += `<div class="card">
        <div class="card__border" id="img-item">
            <img class="card__img" src="${element.image}" alt="${element.name}"/>
        </div>
        <div class="card__item">
            <div class="card__item-name">
                <span>${element.name}</span>
            </div>
            <div class="card__item-value">
                <span>R$${element.price}</span>
            </div>
            <div class="card__item-buy">
              <button>COMPRAR</button>
            </div>
        </div>
    </div>`;
  });

  document.getElementById("itens").innerHTML = itensParaRenderizar;
}

function buscar(item) {
  let itensParaRenderizar = "";
  let valorBuscado = "";

  item
    ? (valorBuscado = item)
    : (valorBuscado = document.getElementById("buscar").value);

  this.itensEmExibicao.filter((item) => {
    let itemEncontrado = item.name
      .toLowerCase()
      .includes(valorBuscado.toLowerCase());
    if (itemEncontrado) {
      itensParaRenderizar += `<div class="card">
          <div class="card__border" id="img-item">
              <img class="card__img" src="${item.image}" alt="${item.name}"/>
          </div>
          <div class="card__item">
              <div class="card__item-name">
                  <span>${item.name}</span>
              </div>
              <div class="card__item-value">
                  <span>R$${item.price}</span>
              </div>
              <div class="card__item-buy">
                <button>COMPRAR</button>
              </div>
          </div>
      </div>`;
    }

    return (document.getElementById("itens").innerHTML = itensParaRenderizar);
  });
}

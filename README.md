# Documentação
O teste foi realizado utilizando (HTML, CSS e JavaScript). 
## Gulp
adicionado para automatizar a compilação dos arquivos e mimifica-los.
## Sass
adicionado para reutilizar variaveis, possibilitar o uso de mixins, organização de estutura css entre outros.


## Como iniciar o desenvolvimento
- Instale as dependências
```
npm i
```
- Rode o style para aplicar os estilos ao codigo
```
npm run style
```
- Rode a aplicação
```
npm run gulp
```
- A api deve estar rodando para que os dados sejam carregados

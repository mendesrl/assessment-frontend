const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");

function style() {
  return gulp
    .src("./public/app/scss/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss([cssnano()]))
    .pipe(gulp.dest("./public/app/dist/css"))
    .pipe(browserSync.stream());
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./public",
    },
  });
  gulp.watch("./public/app/scss/**/*.scss", style);
  gulp.watch("./public/*.html").on("change", browserSync.reload);
  gulp.watch("./public/app/js/**/*.js").on("change", browserSync.reload);
}

exports.style = style;
exports.watch = watch;
